import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'primeng/api';
import { User } from '../../model/User';
import { AuthService } from '../../service/auth/auth.service';
import { UserService } from '../../service/user/user.service';

@Component({
  selector: 'ngx-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  errorMessage = '';
  user: User;
  userForm: FormGroup;
  file: string = '';
  image: any;
  files: File;
  roles = [{value:'ROLE_USER', name: 'Organisateur'}, {value:'ROLE_SPORT', name: 'Sporitf'}, {value:'ROLE_GUEST', name: 'Visiteur'}];
  types = [{value:'natation', name: 'natation'},{value:'cyclisme', name: 'Cyclisme'}];
  selectedCountry= [];
  isUserFormSubmitted: boolean = false;
  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private spinner: NgxSpinnerService,
    private userService: UserService
  ) {
    this.user = new User();
  }

  ngOnInit(): void {
    this.createUserForm();
  }

  createUserForm() {
    this.userForm = this.formBuilder.group({
      email: [, [Validators.required, Validators.email]],
      firstName: [],
      lastName: [, [Validators.required]],
      adresse: [, [Validators.required]],
      password: [, [Validators.required]],
      confirmPassword: [, [Validators.required]],
      login: [, [Validators.required]],
      sportType: [, [Validators.required]],
      role: [, [Validators.required]]
    });
  }

  addNotif(type: string, summary: string) {
    this.messageService.add({ severity: type, summary: summary });
  }

  get formControls() {
    return this.userForm.controls;
  }

  onSubmit() {
    this.isUserFormSubmitted = true;
    console.log();
    
    if (this.userForm.invalid) {
      return;
    } else {
      console.log(this.userForm.value);
      
      this.spinner.show();
      this.authService.registerUser(this.userForm.value).subscribe(
        (val) => {
          this.addNotif('success', 'Création effectuée avec succès');
          if(this.files != null ){
            this.userService.uploadFile(this.files, val.id).subscribe(element=>{
              this.spinner.hide();
            })
          }
          this.back();
          this.spinner.hide();
        },
        (err) => {
          this.addNotif(
            'error',
            err.error
          );
          this.spinner.hide();
        }
      );
    }
  }

  back() {
    this.router.navigate(['/auth']);
  }

  onFileChange(ev) {
    if (ev.target.files[0] != null) {
      this.files = ev.target.files[0];
      if (
        this.files.name.toLocaleLowerCase().endsWith('.png') ||
        this.files.name.toLocaleLowerCase().endsWith('.jpg') ||
        this.files.name.toLocaleLowerCase().endsWith('.gif') ||
        this.files.name.toLocaleLowerCase().endsWith('.jpeg')
      ) {
        let name: String = '';
        name = this.files.name;
        console.log(name);

        name = name.replace(' ', '_');
        name.split(' ').forEach((vam) => {
          name = name.replace(' ', '_');
        });
        const reader = new FileReader();
        reader.onload = () => {
          this.image = reader.result;
        };
        reader.readAsDataURL(this.files);
      } else {
      }
    }
  }




}
