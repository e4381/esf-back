import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { AuthService } from '../../service/auth/auth.service';

@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private formBuilder: FormBuilder,
    private messageService: MessageService,
    private authService: AuthService,
    private router: Router) {
  }

  loginform: FormGroup;
  isSubmitted: Boolean = false;

  ngOnInit(): void {
    this.loginform = this.formBuilder.group({
      username: [, [Validators.required]],
      password: [, [Validators.required]],
    });
  }

  get formControls() { return this.loginform.controls; }

  doLogin() {
    this.isSubmitted = true;
    if (this.loginform.invalid) {
      this.messageService.add({ severity: 'error', detail: 'Veuillez renseigner le login et le mot de passe' });
      return;
    }

    this.authService.authenticateUser(this.loginform.value).subscribe(res => {
      console.log(res);
      if(res.enable){
        localStorage.setItem("UserId", res.token)
        localStorage.setItem("role", res.role)
        this.router.navigateByUrl("/pages/gestionCompetition");
      }else{
        this.messageService.add({ severity: 'error', detail: "Vous n'avez pas accès a votre compte pour le moment" });
      }

    }, error => {
      this.messageService.add({ severity: 'error', detail: "Veuillez vérifier votre login ou mot de passe" });
    })

  }

  forgetPassWord() {

  }

  createAccount() {
    this.router.navigateByUrl("/auth/register");
  }

  goHome() {
    this.router.navigateByUrl("/home");
  }

}
