import { Component, OnInit } from '@angular/core';
import { Competition } from '../model/Competition';
import { Rss } from '../model/Rss';
import { CompetitionService } from '../service/competition/competition.service';

@Component({
  selector: 'ngx-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private competitionService: CompetitionService) { }
  images: any[] =[
    {
      "previewImageSrc": "../../assets/images/bandeau_214.png",
      "thumbnailImageSrc": "../../assets/images/bandeau_214.png",
      "alt": "Description for Image 3",
      "title": "Title 3"
  },
    {
        "previewImageSrc": "../../assets/images/race-801940_1920-1400x500.jpg",
        "thumbnailImageSrc": "../../assets/images/race-801940_1920-1400x500.jpg",
        "alt": "Description for Image 1",
        "title": "Title 1"
    },
    {
        "previewImageSrc": "../../assets/images/trois-balle-tennis-bleu-1400x500-1.jpg",
        "thumbnailImageSrc": "../../assets/images/trois-balle-tennis-bleu-1400x500-1.jpg",
        "alt": "Description for Image 2",
        "title": "Title 2"
    }
    ];

  responsiveOptions:any[] = [
  ];
  compertitions : Competition[] = [];
  findAllCompetitions() {
    this.competitionService.findAllCompetition().subscribe(val=>{
      this.compertitions = val.slice(0, 3);
    })
  }

  ngOnInit(): void {
    this.findAllCompetitions();
    this.makeFileRequest();
  }

  loadRss() {
    this.competitionService.loadRss().subscribe(val =>{
      console.log(val);
      console.log(val.rss);
      console.log(val.channel);
    })
  }
  rssList : Array<Rss>= [];

  makeFileRequest() {
    let xhr = new XMLHttpRequest()
    xhr.open("GET", "https://cors-anywhere.herokuapp.com/https://rmcsport.bfmtv.com/rss/fil-sport");
    xhr.setRequestHeader('Access-Control-Allow-Origin', '*');
    xhr.setRequestHeader('Access-Control-Allow-Methods', 'GET,POST,OPTIONS,DELETE,PUT');
    xhr.setRequestHeader('Access-Control-Allow-Headers', ' Content-Type');
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send();
    xhr.onload =  () => {
      let parser = new DOMParser();
      let xmlDoc = parser.parseFromString(xhr.response,"text/xml");
      console.log(xmlDoc);
      let rssList : Array<Rss>= [];
      for (let index = 2; index < 8; index++) {
        let rss : Rss= new Rss();
        rss.title = xmlDoc.getElementsByTagName("title")[index].childNodes[0].nodeValue;
        rss.description = xmlDoc.getElementsByTagName("description")[index-1].childNodes[0].nodeValue;
        rss.link = xmlDoc.getElementsByTagName("link")[index-1].childNodes[0].nodeValue;
        rss.pubDate = xmlDoc.getElementsByTagName("pubDate")[index-2].nodeValue;
        rss.enclosure = xmlDoc.getElementsByTagName("enclosure")[index-2].getAttribute('url');
        let indexOf = rss.description.indexOf("<img");
        rss.description = rss.description.substring(0, indexOf)
        rssList.push(rss);
      }
      console.log(rssList);
      this.rssList = [...rssList];
      return rssList;
    }
  }

  isLoggedIn() {
    return localStorage.getItem("UserId") != null;
  }
}
