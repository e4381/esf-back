import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from } from 'rxjs';
import { environment } from '../../../environments/environment';
import { Competition } from '../../model/Competition';
import { Rss } from '../../model/Rss';

@Injectable({
  providedIn: 'root'
})
export class CompetitionService {

  constructor(private httpclient: HttpClient) { }

  header = new HttpHeaders(
    {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': '*'
    }
  );

  findAllCompetition() {
    return this.httpclient.get<any[]>(environment.api_url + "competitions");
  }

  findCompetitionById(competitionId: string) {
    return this.httpclient.get<any>(environment.api_url + "competitions/" + competitionId);
  }

  createCompetition(competition: Competition) {
    return this.httpclient.post<any>(environment.api_url + "competitions", competition);
  }

  deleteCompetition(competitionId: string) {
    return this.httpclient.delete<any>(environment.api_url + "competitions/" + competitionId);
  }

  uploadFile(file: File, userId: string) {
    let formdata: FormData = new FormData();
    formdata.append('file', file);
    return this.httpclient.post<any>(
      environment.api_url + 'competitions/upload/' + userId, formdata
    );
  }

  loadRss() {
    return this.httpclient.get<any>("https://cors-anywhere.herokuapp.com/https://rmcsport.bfmtv.com/rss/fil-sport");
  }

}
