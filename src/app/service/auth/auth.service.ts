import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Authentification } from '../../model/Authentification';
import { User } from '../../model/User';
import jwt_decode from "jwt-decode";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private httpclient: HttpClient) { }

  authenticateUser(authentification: Authentification) {
    return this.httpclient.post<any>(environment.api_url + "auth/signin", authentification);
  }

  registerUser(User: User) {
    return this.httpclient.post<any>(environment.api_url + "auth/signup", User);
  }

  getUserId(): any {
    let token: any = jwt_decode(localStorage.getItem("UserId"));
    return token.jti;
  }

  isAuthorized(userRole : string) {
    let role = localStorage.getItem("role")
    return userRole == role;
  }
}
