import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { TrainingGroup } from '../../model/TrainingGroup';

@Injectable({
  providedIn: 'root'
})
export class GroupService {

  constructor(private httpclient: HttpClient) { }

  findAllTrainingGroup() {
    return this.httpclient.get<TrainingGroup[]>(environment.api_url + "groups");
  }

  findTrainingGroupById(id: string) {
    return this.httpclient.get<TrainingGroup>(environment.api_url + "groups/" + id);
  }

  createTrainingGroup(trainingGroup:TrainingGroup) {
    return this.httpclient.post<TrainingGroup>(environment.api_url + "groups", trainingGroup);
  }
  
  deleteTrainingGroupById(id: string) {
    return this.httpclient.delete<TrainingGroup>(environment.api_url + "groups/" + id);
  }
}
