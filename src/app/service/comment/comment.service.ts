import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Comment } from '../../model/Comment';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(private httpclient: HttpClient) { }

  findAllCommetByCompetitionIdAndUserId(competitionId: string, userId: string) {
    return this.httpclient.get<Comment[]>(environment.api_url + "commets/competition/"+ competitionId+ "/user/"+ userId);
  }

  createComment(comment: Comment) {
    return this.httpclient.post<Comment>(environment.api_url + "commets", comment);
  }

  CountComment() {
    return this.httpclient.get<any[]>(environment.api_url + "commets/top");
  }

}
