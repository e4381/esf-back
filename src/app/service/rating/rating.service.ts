import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { Rating } from '../../model/Rating';

@Injectable({
  providedIn: 'root'
})
export class RatingService {

  constructor(private httpclient: HttpClient) { }

  findAllRatingByCompetitionIdAndUserId(competitionId: string, userId: string) {
    return this.httpclient.get<Rating>(environment.api_url + "ratings/competition/"+ competitionId+ "/user/"+ userId);
  }

  createRating(rating: Rating) {
    return this.httpclient.post<Rating>(environment.api_url + "ratings", rating);
  }

  findTopRating() {
    return this.httpclient.get<any[]>(environment.api_url + "ratings/top");
  }
}
