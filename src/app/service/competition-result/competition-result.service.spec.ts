import { TestBed } from '@angular/core/testing';

import { CompetitionResultService } from './competition-result.service';

describe('CompetitionResultService', () => {
  let service: CompetitionResultService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CompetitionResultService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
