import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { CompetitionResult } from '../../model/CompetitionResult';

@Injectable({
  providedIn: 'root'
})
export class CompetitionResultService {

  constructor(private httpclient: HttpClient) { }

  findAllByUserId(userId: string) {
    return this.httpclient.get<CompetitionResult[]>(environment.api_url + "competition_result/user/"+ userId);
  }

  findAllCompetitionResultByCompetitionId(id: string) {
    return this.httpclient.get<CompetitionResult[]>(environment.api_url + "competition_result/competition/"+ id);
  }

  findAllCompetitionResult() {
    return this.httpclient.get<CompetitionResult[]>(environment.api_url + "competition_result");
  }

  findCompetitionResultByCompetitionIdAndUserId(competitionId: string, userId: string) {
    return this.httpclient.get<any>(environment.api_url + "competition_result/competition/"+ competitionId+ "/user/"+ userId);
  }

  createCompetitionResult(competitionResult: CompetitionResult) {
    return this.httpclient.post<any>(environment.api_url + "competition_result" , competitionResult);
  }

  deletePartisipation(id: string) {
    return this.httpclient.delete<any>(environment.api_url + "competition_result/"+ id);
  }
}
