import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AnySrvRecord } from 'dns';
import { environment } from '../../../environments/environment';
import { User } from '../../model/User';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpclient: HttpClient) { }

  findAllUserByRole(role: string) {
    return this.httpclient.get<User[]>(environment.api_url + "users/role/" + role);
  }

  findAllUser() {
    return this.httpclient.get<User[]>(environment.api_url + "users");
  }

  deleteUserById(id: string) {
    return this.httpclient.delete<User[]>(environment.api_url + "users/" + id);
  }

  findUserById(id: string) {
    return this.httpclient.get<User>(environment.api_url + "users/" + id);
  }

  findUsersByTrainingGroupAndUserId(groupId: string, userId: string) {
    return this.httpclient.get<User>(environment.api_url + "users/" + userId + "/trainingGroup/" + groupId);
  }

  findUsersByTrainingGroup(groupId: string) {
    return this.httpclient.get<User[]>(environment.api_url + "users/trainingGroup/" + groupId);
  }

  UpdateUser(user: User) {
    return this.httpclient.put<User>(environment.api_url + "users", user);
  }

  disableUser(userId: string) {
    return this.httpclient.patch<User>(environment.api_url + "users/disableUser/" + userId, null);
  }

  enableUser(userId: string) {
    return this.httpclient.patch<User>(environment.api_url + "users/enableUser/" + userId, null);
  }

  uploadFile(file: File, userId: string) {
    let formdata: FormData = new FormData();
    formdata.append('file', file);
    return this.httpclient.post<any>(
      environment.api_url + 'users/upload/' + userId, formdata
    );
  }

  loadFile() {
    return this.httpclient.get<any>(
      environment.api_url + '/users/files'
    );
  }


}
