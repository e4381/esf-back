import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { AuthService } from '../../service/auth/auth.service';
import { UserService } from '../../service/user/user.service';

@Component({
  selector: 'ngx-user-profil',
  templateUrl: './user-profil.component.html',
  styleUrls: ['./user-profil.component.scss']
})
export class UserProfilComponent implements OnInit {

  constructor(private userService: UserService,
    private authService: AuthService,
    private messageService: MessageService,
    private router: Router) { }

  ngOnInit(): void {
  }

  disable() {
    this.userService.disableUser(this.authService.getUserId()).subscribe(val =>{
      this.messageService.add({ severity: 'success', summary: "votre compte est déactiver" });
      this.router.navigateByUrl("/auth");
    })
  }

  delete() {
    this.userService.deleteUserById(this.authService.getUserId()).subscribe(val =>{
      this.messageService.add({ severity: 'success', summary: "votre compte est supprimer" });
      this.router.navigateByUrl("/auth");
    })
  }
  
  isAdmin(){
    return localStorage.getItem("role") == "ROLE_ADMIN";
  }

}
