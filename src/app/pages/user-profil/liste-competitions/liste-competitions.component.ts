import { Component, OnInit } from '@angular/core';
import { CompetitionResult } from '../../../model/CompetitionResult';
import { AuthService } from '../../../service/auth/auth.service';
import { CompetitionResultService } from '../../../service/competition-result/competition-result.service';

@Component({
  selector: 'ngx-liste-competitions',
  templateUrl: './liste-competitions.component.html',
  styleUrls: ['./liste-competitions.component.scss']
})
export class ListeCompetitionsComponent implements OnInit {

  constructor(private authService: AuthService,
    private competitionResultService: CompetitionResultService) { }

  competitionResults: CompetitionResult[] = [];
  cols: any[] = [
    { field: "name", header: "name" },
    { field: "sportType", header: "sportType" },
    { field: "Action", header: "Action" }];

  ngOnInit(): void {
    this.getAllCompetitionResultsByUserId();
  }

  getAllCompetitionResultsByUserId() {
    this.competitionResultService.findAllByUserId(this.authService.getUserId()).subscribe(val =>{
      this.competitionResults = val;
    })
  }

  updateScroe(competitionResult: CompetitionResult) {
    console.log(competitionResult);
    this.competitionResultService.createCompetitionResult(competitionResult).subscribe();
  }

}
