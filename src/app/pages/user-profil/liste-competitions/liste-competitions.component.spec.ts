import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListeCompetitionsComponent } from './liste-competitions.component';

describe('ListeCompetitionsComponent', () => {
  let component: ListeCompetitionsComponent;
  let fixture: ComponentFixture<ListeCompetitionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListeCompetitionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListeCompetitionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
