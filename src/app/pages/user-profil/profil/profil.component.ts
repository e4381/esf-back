import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'primeng/api';
import { User } from '../../../model/User';
import { AuthService } from '../../../service/auth/auth.service';
import { UserService } from '../../../service/user/user.service';

@Component({
  selector: 'ngx-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss']
})
export class ProfilComponent implements OnInit {

  
  errorMessage = '';
  user: User;
  userForm: FormGroup;
  loading: boolean = false;
  file: string = '';
  image: any;
  files: File;
  roles = [{ value: 'ROLE_GUEST', name: 'Visiteur' }, { value: 'ROLE_SPORT', name: 'Sportif' }];
  types = [{ value: 'natation', name: 'natation' }, { value: 'cyclisme', name: 'Cyclisme' }];
  selectedCountry = [];
  isUserFormSubmitted: boolean = false;
  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private spinner: NgxSpinnerService,
    private activatedRoute: ActivatedRoute,
    private userService: UserService
  ) {
    this.user = new User();
  }

  ngOnInit(): void {
    this.createUserForm();
    this.getUser();
  }

  createUserForm(user?: User) {
    this.userForm = this.formBuilder.group({
      id: [user?.id],
      email: [user?.email, [Validators.required, Validators.email]],
      firstName: [user?.firstName, [Validators.required]],
      lastName: [user?.lastName, [Validators.required]],
      adresse: [user?.adresse, [Validators.required]],
      login: [user?.login, [Validators.required]],
      sportType: ["Admin", [Validators.required]],
      role: [user?.role, [Validators.required]],
      password: [user?.password, [Validators.required]],
    });
  }

  getUser() {
    this.userService.findUserById(this.authService.getUserId()).subscribe(val =>{
      console.log(val);
      this.createUserForm(val);
      if(val.logo != null) {
        this.image = val.logo;
      }
    })
  }

  addNotif(type: string, summary: string) {
    this.messageService.add({ severity: type, summary: summary });
  }

  get formControls() {
    return this.userForm.controls;
  }

  onSubmit() {
    this.isUserFormSubmitted = true;
    console.log();

    if (this.userForm.invalid) {
      return;
    } else {
      console.log(this.userForm.value);

      this.spinner.show();
      this.userService.UpdateUser(this.userForm.value).subscribe(
        (val) => {
          this.userService.uploadFile(this.files, val.id).subscribe()
          this.addNotif('success', 'Modification effectuée avec succès');
          // this.back();
        },
        (err) => {
          this.addNotif(
            'error',
            'Un problème est survenu lors de la modification veuillez réessayer plus tard.'
          );
        }
      );
    }
  }

  back() {
    this.router.navigate(['/pages/gestionSportif']);
  }

  onFileChange(ev) {
    if (ev.target.files[0] != null) {
      this.files = ev.target.files[0];
      if (
        this.files.name.toLocaleLowerCase().endsWith('.png') ||
        this.files.name.toLocaleLowerCase().endsWith('.jpg') ||
        this.files.name.toLocaleLowerCase().endsWith('.gif') ||
        this.files.name.toLocaleLowerCase().endsWith('.jpeg')
      ) {
        let name: String = '';
        name = this.files.name;
        console.log(name);

        name = name.replace(' ', '_');
        name.split(' ').forEach((vam) => {
          name = name.replace(' ', '_');
        });
        const reader = new FileReader();
        reader.onload = () => {
          this.image = reader.result;
        };
        reader.readAsDataURL(this.files);
      } else {
      }
    }
  }

  isAdmin(){
    return localStorage.getItem("role") == "ROLE_ADMIN";
  }

}
