import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { TrainingGroup } from '../../model/TrainingGroup';
import { User } from '../../model/User';
import { AuthService } from '../../service/auth/auth.service';
import { GroupService } from '../../service/group/group.service';
import { UserService } from '../../service/user/user.service';

@Component({
  selector: 'ngx-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.scss']
})
export class GroupComponent implements OnInit {

  constructor(private groupService: GroupService,
    private route: Router,
    private authService: AuthService,
    private userService: UserService,
    private messageService: MessageService) { }

  trainingGroups: TrainingGroup[] = [];
  loading: boolean = true;
  diplay: boolean = false;
  displayDetails: boolean = false;
  currnetTrainingGroup: TrainingGroup;
  user: User = new User();
  currentGroup: TrainingGroup ;
  cols: any[] = [
    { field: "name", header: "name" },
    { field: "sportType", header: "sportType" },
    { field: "Action", header: "Action" }];

  ngOnInit(): void {
    this.getGroups();
    this.getUser();
  }

  getGroups() {
    this.groupService.findAllTrainingGroup().subscribe(res => {
      this.trainingGroups = res;
      this.trainingGroups.forEach(trainingGroup => {
        this.userService.findUsersByTrainingGroupAndUserId(trainingGroup.id, this.authService.getUserId()).subscribe(val => {
          trainingGroup.joined = val
        })
      })
      this.loading = false;
    })
  }

  addTrainingGroup() {
    this.route.navigateByUrl("/pages/gestionGroupe/create")
  }

  editTrainingGroup(TrainingGroup: TrainingGroup) {
    this.route.navigateByUrl("/pages/gestionGroupe/update/" + TrainingGroup?.id)
  }

  deleteTrainingGroup(TrainingGroup: TrainingGroup) {
    this.currnetTrainingGroup = TrainingGroup;
    this.diplay = true;
  }

  confirme() {
    this.groupService.deleteTrainingGroupById(this.currnetTrainingGroup.id).subscribe(val =>{
      console.log(val);
      this.trainingGroups = this.trainingGroups.filter(el => el.id != this.currnetTrainingGroup.id);
      this.diplay = false;
    })
  }

  getUser() {
    this.userService.findUserById(this.authService.getUserId()).subscribe(val=>{
      this.user= val;
    })
  }

  join(group: TrainingGroup) {
    this.user.trainingGroup = group;
    this.userService.UpdateUser(this.user).subscribe(val =>{
      this.addNotif('success', "vous avez rejoint ce groupe avec succès"); 
      group.joined = this.user;
      this.getGroups();
    })
  }

  cancelJoin(group: TrainingGroup) {
    this.user.trainingGroup = null;
    this.userService.UpdateUser(this.user).subscribe(val =>{
      this.addNotif('success', "vous avez sortie du groupe avec succès"); 
      group.joined = null;
    })
  }

  diplayUsers: boolean = false;
  users: User[] = [];
  groupDetails(trainingGroup: TrainingGroup) {
    
    this.currentGroup = trainingGroup;
    this.userService.findUsersByTrainingGroup(trainingGroup.id).subscribe(val =>{
      this.users = val;
      this.diplayUsers = true;
    })
    
  }

  isAutorised(){
    return localStorage.getItem("role") == "ROLE_USER";
  }

  isAdmin(){
    return localStorage.getItem("role") == "ROLE_ADMIN";
  }

  isSportif(){
    return localStorage.getItem("role") == "ROLE_SPORT";
  }
  
  addNotif(type: string, summary: string) {
    this.messageService.add({ severity: type, summary: summary });
  }
}
