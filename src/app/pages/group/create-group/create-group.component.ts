import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import { TrainingGroup } from '../../../model/TrainingGroup';
import { GroupService } from '../../../service/group/group.service';

@Component({
  selector: 'ngx-create-group',
  templateUrl: './create-group.component.html',
  styleUrls: ['./create-group.component.scss']
})
export class CreateGroupComponent implements OnInit {

  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private TrainingGroupService: GroupService,
    private messageService: MessageService,
    private activatedRouter: ActivatedRoute) { }

  loading: boolean = false;
  TrainingGroupForm: FormGroup;
  isTrainingGroupFormSubmitted: boolean = false;
  types = [{ value: 'natation', name: 'natation' }, { value: 'cyclisme', name: 'Cyclisme' }];
  id = this.activatedRouter.snapshot.params["id"];
  ngOnInit(): void {
    this.setTrainingGroupForm();
    if (this.id) this.getTrainingGroup();
  }

  setTrainingGroupForm(TrainingGroup?: TrainingGroup) {
    this.TrainingGroupForm = this.formBuilder.group({
      id: [TrainingGroup?.id],
      name: [TrainingGroup?.name, [Validators.required]],
      description: [TrainingGroup?.description, [Validators.required]],
      sportType: [TrainingGroup?.sportType, [Validators.required]],
    })
  }

  getTrainingGroup() {
    this.TrainingGroupService.findTrainingGroupById(this.id).subscribe(val => {
      this.setTrainingGroupForm(val);
    })
  }

  get formControls() { return this.TrainingGroupForm.controls; }

  returntolist() {
    this.router.navigateByUrl("/pages/gestionGroupe")
  }

  addTrainingGroup() {
    this.isTrainingGroupFormSubmitted = true;
    if (this.TrainingGroupForm.invalid) return;
    this.TrainingGroupService.createTrainingGroup(this.TrainingGroupForm.value).subscribe(res => {
      this.returntolist();
    }, err => {
      this.messageService.add({ severity: 'error', detail: "Un problème est survenu lors de la modification veuillez réessayer plus tard." });
    })
  }
  
  isAutorised(){
    return localStorage.getItem("role") == "ROLE_USER";
  }
}
