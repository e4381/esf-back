import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CompetitionMapComponent } from './competition-map.component';

describe('CompetitionMapComponent', () => {
  let component: CompetitionMapComponent;
  let fixture: ComponentFixture<CompetitionMapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CompetitionMapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CompetitionMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
