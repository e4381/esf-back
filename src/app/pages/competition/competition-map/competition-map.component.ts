import { Component, OnInit } from '@angular/core';

import * as L from 'leaflet';
import { Competition } from '../../../model/Competition';
import { CompetitionService } from '../../../service/competition/competition.service';
@Component({
  selector: 'ngx-competition-map',
  templateUrl: './competition-map.component.html',
  styleUrls: ['./competition-map.component.scss']
})
export class CompetitionMapComponent implements OnInit {

  constructor(private competitionService: CompetitionService) { }

  LeafIcon = L.Icon.extend({
    options: {
      iconSize: [38, 35]
    }
  });
  greenIcon = new this.LeafIcon({
    iconUrl: '../../../assets/images/pin.png',
    shadowUrl: ''
  })

  options2 = {
    layers: [
      
    ],
    zoom: 7,
    center: L.latLng({ lat: 48.873642, lng: 2.307692 }),
  };
  options ;

  currnetCompetition : Competition;
  displayDetails :boolean = false;
  competitions: Competition[] = [];

  ngOnInit(): void {
    this.getCompetitions();
  }
  

  getCompetitions() {
    this.competitionService.findAllCompetition().subscribe(res => {
      this.competitions = res;
      this.competitions.forEach(competition => {
        let layer = L.marker([Number(competition.latitude), Number(competition.longitude)], { icon: this.greenIcon })
        .bindTooltip(competition.name).openTooltip()
        .on('click', e =>{
          this.currnetCompetition = competition;
          this.displayDetails = true;
          console.log(this.currnetCompetition);
          
        });
        this.options2.layers.push(layer);
      })

      this.options2.layers.push(L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: '...' }))
      this.options = this.options2;
    })
  }

  closeDetails() {
    this.displayDetails = false;
  }
  
}
