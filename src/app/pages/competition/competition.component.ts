import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Competition } from '../../model/Competition';
import { CompetitionResult } from '../../model/CompetitionResult';
import { User } from '../../model/User';
import { AuthService } from '../../service/auth/auth.service';
import { CompetitionResultService } from '../../service/competition-result/competition-result.service';
import { CompetitionService } from '../../service/competition/competition.service';

@Component({
  selector: 'ngx-competition',
  templateUrl: './competition.component.html',
  styleUrls: ['./competition.component.scss']
})
export class CompetitionComponent implements OnInit {

  constructor(private competitionService: CompetitionService,
    private route: Router,
    private competitionResultService: CompetitionResultService,
    private authService: AuthService,
    private messageService: MessageService) { }

  competitions: Competition[] = [];
  loading: boolean = true;
  diplay: boolean = false;
  reload: boolean = true;
  displayDetails: boolean = false;
  currnetCompetition: Competition;
  cols: any[] = [
    { field: "name", header: "name" },
    { field: "sportType", header: "sportType" },
    { field: "Action", header: "Action" },
    { field: "startEvent", header: "startEvent" },
    { field: "endEvent", header: "endEvent" }];

  ngOnInit(): void {
    this.getCompetitions();
  }

  getCompetitions() {
    this.competitionService.findAllCompetition().subscribe(res => {
      this.competitions = res;
      this.competitions.forEach(competition => {
        this.competitionResultService.findCompetitionResultByCompetitionIdAndUserId(competition.id, this.authService.getUserId()).subscribe(val => {
          competition.participated = val
        })
      })
      this.loading = false;
    })
  }

  addCompetition() {
    this.route.navigateByUrl("/pages/gestionCompetition/create")
  }

  editCompetation(competition: Competition) {
    this.route.navigateByUrl("/pages/gestionCompetition/update/" + competition?.id)
  }

  deleteCompetation(competition: Competition) {
    this.currnetCompetition = competition;
    this.diplay = true;
  }

  confirme() {
    this.competitionService.deleteCompetition(this.currnetCompetition.id).subscribe(val => {
      this.competitions = this.competitions.filter(element => element.id != this.currnetCompetition.id);
      this.diplay = false;
    })
  }

  competationDetails(competition: Competition) {
    this.currnetCompetition = competition;
    this.displayDetails = true;
  }

  closeDetails() {
    this.displayDetails = false;
  }

  participate(competition: Competition) {
    let competationResult: CompetitionResult = new CompetitionResult();
    competationResult.competition = competition;
    let user: User = new User();
    user.id = this.authService.getUserId();
    competationResult.user = user;
    this.competitionResultService.createCompetitionResult(competationResult).subscribe(val => {
      competition.participated = val;
      this.addNotif('success', 'Votre participation a été ajoutée dans votre calendrier'); 
    })
  }

  cancelParticipate(competition: Competition) {
    this.competitionResultService.deletePartisipation(competition.participated.id).subscribe(val => {
      competition.participated = null;
      this.addNotif('success', 'Votre participation a été retirée dans votre calendrier'); 

    })
  }

  isAutorised(){
    return localStorage.getItem("role") == "ROLE_ADMIN" || localStorage.getItem("role") == "ROLE_USER";
  }

  isUser(){
    return localStorage.getItem("role") == "ROLE_USER";
  }

  addNotif(type: string, summary: string) {
    this.messageService.add({ severity: type, summary: summary });
  }

  isValidDate(endDate: Date) {
    let date = new Date(endDate);
    return date?.getTime() >= new Date().getTime();
  }
}
