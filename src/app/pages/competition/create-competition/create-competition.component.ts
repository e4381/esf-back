import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Competition } from '../../../model/Competition';
import { CompetitionService } from '../../../service/competition/competition.service';

@Component({
  selector: 'ngx-create-competition',
  templateUrl: './create-competition.component.html',
  styleUrls: ['./create-competition.component.scss']
})
export class CreateCompetitionComponent implements OnInit {

  constructor(private router: Router,
    private formBuilder: FormBuilder,
    private competitionService: CompetitionService,
    private messageService: MessageService,
    private activatedRouter: ActivatedRoute) { }

  loading: boolean = false;
  hasValidDate: boolean = true;
  CompetitionForm: FormGroup;
  isCompetitionFormSubmitted: boolean = false;
  types = [{ value: 'natation', name: 'natation' }, { value: 'cyclisme', name: 'Cyclisme' }];
  id = this.activatedRouter.snapshot.params["id"];

  ngOnInit(): void {
    this.setCompetitionForm();
    if (this.id) this.getCompetition();
  }

  setCompetitionForm(competition?: Competition) {
    this.CompetitionForm = this.formBuilder.group({
      id: [competition?.id],
      name: [competition?.name, [Validators.required]],
      description: [competition?.description, [Validators.required]],
      sportType: [competition?.sportType, [Validators.required]],
      startEvent: [competition ? new Date(competition?.startEvent) : null, [Validators.required]],
      endEvent: [competition ? new Date(competition?.endEvent) : null, [Validators.required]],
      latitude: [competition?.latitude, [Validators.required]],
      longitude: [competition?.longitude, [Validators.required]],
      logo: [competition?.logo]
    })
  }

  getCompetition() {
    this.competitionService.findCompetitionById(this.id).subscribe(val => {
      this.setCompetitionForm(val);
      if(val.logo != null) {
        this.image = val.logo;
      }
    })
  }

  get formControls() { return this.CompetitionForm.controls; }

  returntolist() {
    this.router.navigateByUrl("/pages/gestionCompetition")
  }

  addCompetition() {
    this.isCompetitionFormSubmitted = true;
    this.hasValidDate = true;
    if (this.CompetitionForm.invalid) return;
    if(!this.dateValidator(this.CompetitionForm.value.startEvent, this.CompetitionForm.value.endEvent)){
      this.hasValidDate = false;
      return;
    }

    this.competitionService.createCompetition(this.CompetitionForm.value).subscribe(res => {
      if(this.files)this.competitionService.uploadFile(this.files, res.id).subscribe()
      this.returntolist();
    }, err => {
      this.messageService.add({ severity: 'error', detail: "Compétition déja conçu" });
    })
  }

  file: string = '';
  image: any;
  files: File;


  onFileChange(ev) {
    if (ev.target.files[0] != null) {
      this.files = ev.target.files[0];
      if (
        this.files.name.toLocaleLowerCase().endsWith('.png') ||
        this.files.name.toLocaleLowerCase().endsWith('.jpg') ||
        this.files.name.toLocaleLowerCase().endsWith('.gif') ||
        this.files.name.toLocaleLowerCase().endsWith('.jpeg')
      ) {
        let name: String = '';
        name = this.files.name;
        console.log(name);

        name = name.replace(' ', '_');
        name.split(' ').forEach((vam) => {
          name = name.replace(' ', '_');
        });
        const reader = new FileReader();
        reader.onload = () => {
          this.image = reader.result;
        };
        reader.readAsDataURL(this.files);
      } else {
      }
    }
  }

  private dateValidator(StartDate: Date, endDate: Date) {
    console.log(endDate > StartDate);
    
    return endDate > StartDate;
  }
}
