import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Comment } from '../../../model/Comment';
import { Competition } from '../../../model/Competition';
import { User } from '../../../model/User';
import { AuthService } from '../../../service/auth/auth.service';
import { CommentService } from '../../../service/comment/comment.service';
import { UserService } from '../../../service/user/user.service';

@Component({
  selector: 'ngx-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  constructor(private commentService: CommentService,
    private authService: AuthService,
    private userService: UserService) { }

  @Input() comments: Comment[];
  @Input() currentCompetition: Competition;
  @Output() toastOutPut: EventEmitter<any> = new EventEmitter<any>();
  message: string;

  ngOnInit(): void {
    this.getUser();
  }

  displayDetailsAction() {

  }

  addComment() {
    let comment: Comment = new Comment();
    comment.message = this.message;
    comment.user = this.user;
    comment.competition = this.currentCompetition;
    this.commentService.createComment(comment).subscribe(val => {
      this.comments.unshift(val);
    })
  }

  user: User = new User();
  getUser() {
    this.userService.findUserById(this.authService.getUserId()).subscribe(val => {
      this.user = val;
    })
  }

}
