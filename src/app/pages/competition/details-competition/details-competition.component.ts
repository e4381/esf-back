import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Comment } from '../../../model/Comment';
import { Competition } from '../../../model/Competition';
import { CompetitionResult } from '../../../model/CompetitionResult';
import { Rating } from '../../../model/Rating';
import { User } from '../../../model/User';
import { AuthService } from '../../../service/auth/auth.service';
import { CommentService } from '../../../service/comment/comment.service';
import { CompetitionResultService } from '../../../service/competition-result/competition-result.service';
import { RatingService } from '../../../service/rating/rating.service';
import { UserService } from '../../../service/user/user.service';

@Component({
  selector: 'ngx-details-competition',
  templateUrl: './details-competition.component.html',
  styleUrls: ['./details-competition.component.scss']
})
export class DetailsCompetitionComponent implements OnInit {

  constructor(private ratingService: RatingService,
    private commentService: CommentService,
    private authService: AuthService,
    private competitionResultService: CompetitionResultService,
    private userService: UserService) { }

  @Input() displayDetails: boolean = false;
  @Input() currentCompetition: Competition;
  @Output() closeDetails: EventEmitter<Competition> = new EventEmitter<Competition>();
  rating: Rating = new Rating();
  comments: Comment[] = [];
  competitionResults: CompetitionResult[] = [];
  cols: any[] = [
    { field: "name", header: "name" },
    { field: "sportType", header: "sportType" },
    { field: "Action", header: "Action" }];

  ngOnInit(): void {
    this.findCommentByUser();
    this.getUser();
    this.findRatingByUser();
    this.getAllCompetitionResultsByCompetition(this.currentCompetition);
  }

  user: User = new User();
  getUser() {
    this.userService.findUserById(this.authService.getUserId()).subscribe(val=>{
      this.user= val;
    })
  }

  SaveRating() {
    if(this.rating.id == null) {
      this.rating.user = this.user;
      this.rating.competition = this.currentCompetition
    }
    this.ratingService.createRating(this.rating).subscribe(val =>{
      console.log(val);
    })
    this.closeDetails.emit(this.currentCompetition);
  }

  findCommentByUser() {
    this.commentService.findAllCommetByCompetitionIdAndUserId(this.currentCompetition.id, this.authService.getUserId()).subscribe(val =>{
      this.comments = val;
    })
  }

  findRatingByUser() {
    this.ratingService.findAllRatingByCompetitionIdAndUserId(this.currentCompetition.id, this.authService.getUserId()).subscribe(val =>{
      if(val) this.rating = val
    })
  }

  getAllCompetitionResultsByCompetition(competition: Competition) {
    this.competitionResultService.findAllCompetitionResultByCompetitionId(competition.id).subscribe(val =>{
      this.competitionResults= val;
    })
  }

}
