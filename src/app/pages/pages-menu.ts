import { NbMenuItem } from '@nebular/theme';


export const MENU_ADMIN: NbMenuItem[] = [
  {
    title: 'dashboard',
    icon: 'shopping-cart-outline',
    link: '/pages/dashboard',
    home: true,
  },
  {
    title: 'Compétition',
    icon: 'award-outline',
    link: '/pages/gestionCompetition',
  },
  {
    title: 'Compétition Map',
    icon: 'map-outline',
    link: '/pages/gestionCompetition/map',
  },
  
  {
    title: 'Calendrier',
    icon: 'calendar-outline',
    link: '/pages/calendar',
  },
  {
    title: "Groupe d'entrainement",
    icon: 'grid-outline',
    link: '/pages/gestionGroupe',
  },
  {
    title: "Gestion Organisteurs",
    icon: 'person-outline',
    link: '/pages/gestionOrganisteur',
  },
  {
    title: "Gestion Sortifs",
    icon: 'people-outline',
    link: '/pages/gestionSportif',
  }

];

export const MENU_GUEST: NbMenuItem[] = [
  {
    title: 'Compétition',
    icon: 'award-outline',
    link: '/pages/gestionCompetition',
  },
  {
    title: 'Compétition Map',
    icon: 'map-outline',
    link: '/pages/gestionCompetition/map',
  }
];

export const MENU_SPORTIF: NbMenuItem[] = [
  {
    title: 'Compétition',
    icon: 'award-outline',
    link: '/pages/gestionCompetition',
  },
  {
    title: 'Compétition Map',
    icon: 'map-outline',
    link: '/pages/gestionCompetition/map',
  },
  
  {
    title: 'Calendrier',
    icon: 'calendar-outline',
    link: '/pages/calendar',
  },
  {
    title: "Groupe d'entrainement",
    icon: 'grid-outline',
    link: '/pages/gestionGroupe',
  }
];

export const MENU_ORGANISATEUR: NbMenuItem[] = [
  {
    title: 'Compétition',
    icon: 'award-outline',
    link: '/pages/gestionCompetition',
  },
  {
    title: 'Compétition Map',
    icon: 'map-outline',
    link: '/pages/gestionCompetition/map',
  },
  {
    title: "Groupe d'entrainement",
    icon: 'grid-outline',
    link: '/pages/gestionGroupe',
  }
];
