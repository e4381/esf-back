import { Component, ViewChild } from '@angular/core';
import { takeWhile } from 'rxjs/operators';
import { OrdersChart } from '../../@core/data/orders-chart';
import { OrderProfitChartSummary, OrdersProfitChartData } from '../../@core/data/orders-profit-chart';
import { ProfitChart } from '../../@core/data/profit-chart';
import { Competition } from '../../model/Competition';
import { TrainingGroup } from '../../model/TrainingGroup';
import { User } from '../../model/User';
import { CommentService } from '../../service/comment/comment.service';
import { CompetitionService } from '../../service/competition/competition.service';
import { GroupService } from '../../service/group/group.service';
import { RatingService } from '../../service/rating/rating.service';
import { UserService } from '../../service/user/user.service';
import { OrdersChartComponent } from './charts-panel/charts/orders-chart.component';
import { ProfitChartComponent } from './charts-panel/charts/profit-chart.component';

@Component({
  selector: 'ngx-ecommerce',
  styleUrls: ['./e-commerce.component.scss'],
  templateUrl: './e-commerce.component.html',
})
export class ECommerceComponent {
  private alive = true;

  chartPanelSummary: OrderProfitChartSummary[];
  period: string = 'week';
  profitChartData: ProfitChart = {chartLabel:[], data:[[],[]]};
  users: User[] = [];
  sportifs: User[] = [];
  compertitions: Competition[] = [];
  groups: TrainingGroup[] = [];
  commtens : any[]= []
  liste: ProfitChart = {chartLabel:[], data:[[],[]]}

  @ViewChild('profitChart', { static: true }) profitChart: ProfitChartComponent;

  constructor(private userService: UserService,
    private competitionService: CompetitionService,
    private groupeService: GroupService,
    private ratingService: RatingService,
    private commentService: CommentService) {
    // this.getProfitChartData(this.period);
    this.findAllUser();
    this.findAllCompetitions();
    this.findAllGroups();
    this.findTopRating();
    this.CountComment();
  }

  findAllUser() {
    this.userService.findAllUser().subscribe(val =>{
      this.users = val;
      this.sportifs = val.filter(el => el.role =="ROLE_SPORT");
    })
  }

  findAllCompetitions() {
    this.competitionService.findAllCompetition().subscribe(val=>{
      this.compertitions = val;
    })
  }

  findAllGroups() {
    this.groupeService.findAllTrainingGroup().subscribe(val=>{
      this.groups = val;
    })
  }

  CountComment() {
    this.commentService.CountComment().subscribe(val =>{
      this.commtens = val
    })
  }

  findTopRating() {
    this.ratingService.findTopRating().subscribe(val =>{
      console.log(val);
      val.forEach(el =>{
        this.liste.chartLabel.push(el[1]);
        this.liste.data[1].push(el[2]);
      })
      console.log(this.liste);
      this.profitChartData = this.liste;
    })
  }

  setPeriodAndGetChartData(value: string): void {
    if (this.period !== value) {
      this.period = value;
    }
    this.getProfitChartData(value);
  }

  getProfitChartData(period: string) {
    let liste: ProfitChart = {chartLabel:['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'], data:[[],[488, 113, 145, 320, 30, 458, 471]]}
    this.profitChartData = liste;
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
