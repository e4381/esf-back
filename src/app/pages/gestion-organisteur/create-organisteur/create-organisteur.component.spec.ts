import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateOrganisteurComponent } from './create-organisteur.component';

describe('CreateOrganisteurComponent', () => {
  let component: CreateOrganisteurComponent;
  let fixture: ComponentFixture<CreateOrganisteurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateOrganisteurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateOrganisteurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
