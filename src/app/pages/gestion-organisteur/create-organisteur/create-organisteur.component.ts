import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { MessageService } from 'primeng/api';
import { User } from '../../../model/User';
import { AuthService } from '../../../service/auth/auth.service';
import { UserService } from '../../../service/user/user.service';

@Component({
  selector: 'ngx-create-organisteur',
  templateUrl: './create-organisteur.component.html',
  styleUrls: ['./create-organisteur.component.scss']
})
export class CreateOrganisteurComponent implements OnInit {

  errorMessage = '';
  user: User;
  userForm: FormGroup;
  loading: boolean = false;
  // country = new Countries().countryList;
  roles = [{ value: 'ROLE_GUEST', name: 'Visiteur' }, { value: 'ROLE_SPORT', name: 'Sportif' }];
  types = [{ value: 'natation', name: 'natation' }, { value: 'cyclisme', name: 'Cyclisme' }];
  selectedCountry = [];
  isUserFormSubmitted: boolean = false;
  constructor(
    private authService: AuthService,
    private router: Router,
    private formBuilder: FormBuilder,
    private messageService: MessageService,
    private spinner: NgxSpinnerService,
    private userService: UserService
  ) {
    this.user = new User();
  }

  ngOnInit(): void {
    this.createUserForm();
  }

  createUserForm() {
    this.userForm = this.formBuilder.group({
      email: [, [Validators.required, Validators.email]],
      firstName: [],
      lastName: [, [Validators.required]],
      adresse: [, [Validators.required]],
      password: [, [Validators.required]],
      confirmPassword: [, [Validators.required]],
      login: [, [Validators.required]],
      sportType: [, [Validators.required]],
      role: ["ROLE_USER", [Validators.required]]
    });
  }

  addNotif(type: string, summary: string) {
    this.messageService.add({ severity: type, summary: summary });
  }

  get formControls() {
    return this.userForm.controls;
  }

  onSubmit() {
    this.isUserFormSubmitted = true;
    console.log();

    if (this.userForm.invalid) {
      return;
    } else {
      console.log(this.userForm.value);

      this.spinner.show();
      this.authService.registerUser(this.userForm.value).subscribe(
        (val) => {
          this.userService.uploadFile(this.files, val.id).subscribe()
          this.addNotif('success', 'Création effectuée avec succès');
          this.back();
        },
        (err) => {
          console.log(err);
          
          this.addNotif(
            'error',
            err.error
          );
        }
      );
    }
  }

  back() {
    this.router.navigate(['/pages/gestionOrganisteur']);
  }

  file: string = '';
  image: any;
  files: File;

  onFileChange(ev) {
    if (ev.target.files[0] != null) {
      this.files = ev.target.files[0];
      if (
        this.files.name.toLocaleLowerCase().endsWith('.png') ||
        this.files.name.toLocaleLowerCase().endsWith('.jpg') ||
        this.files.name.toLocaleLowerCase().endsWith('.gif') ||
        this.files.name.toLocaleLowerCase().endsWith('.jpeg')
      ) {
        let name: String = '';
        name = this.files.name;
        console.log(name);

        name = name.replace(' ', '_');
        name.split(' ').forEach((vam) => {
          name = name.replace(' ', '_');
        });
        const reader = new FileReader();
        reader.onload = () => {
          this.image = reader.result;
        };
        reader.readAsDataURL(this.files);
      } else {
      }
    }
  }
}
