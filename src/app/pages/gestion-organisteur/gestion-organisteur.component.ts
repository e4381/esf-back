import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { User } from '../../model/User';
import { UserService } from '../../service/user/user.service';

@Component({
  selector: 'ngx-gestion-organisteur',
  templateUrl: './gestion-organisteur.component.html',
  styleUrls: ['./gestion-organisteur.component.scss']
})
export class GestionOrganisteurComponent implements OnInit {

  constructor(private userService: UserService,
    private route: Router,
    private messageService: MessageService) { }

  users: User[] = [];
  loading: boolean = true;
  diplay: boolean = false;
  displayDetails: boolean = false;
  currnetUser: User;

  cols: any[] = [
    { field: "name", header: "name" },
    { field: "sportType", header: "sportType" },
    { field: "sportType", header: "sportType" },
    { field: "sportType", header: "sportType" },
    { field: "Action", header: "Action" }];

  ngOnInit(): void {
    this.getSportfs();
  }

  getSportfs() {
    this.userService.findAllUserByRole("ROLE_USER").subscribe(res => {
      this.users = res;
      this.loading = false;
    })
  }

  addSportif() {
    this.route.navigateByUrl("/pages/gestionOrganisteur/create")
  }

  editSportif(TrainingGroup: User) {
    this.route.navigateByUrl("/pages/gestionOrganisteur/update/" + TrainingGroup?.id)
  }

  deleteSportif(user: User) {
    this.currnetUser = user;
    this.diplay = true;
  }

  confirme() {
    this.userService.deleteUserById(this.currnetUser.id).subscribe(val =>{
      console.log(val);
      this.users = this.users.filter(el => el.id != this.currnetUser.id);
      this.diplay = false;
    })
  }

  disable(user: User) {
    this.userService.disableUser(user.id).subscribe(val =>{
      this.addNotif('success', "l'organisateur a été bloquée "); 
      user.isActive = false;
    })
  }

  enableUser(user: User) {
    this.userService.enableUser(user.id).subscribe(val =>{
      this.addNotif('success', "l'organisateur a été débloquée "); 
      user.isActive = true;
    })
  }

  addNotif(type: string, summary: string) {
    this.messageService.add({ severity: type, summary: summary });
  }

}
