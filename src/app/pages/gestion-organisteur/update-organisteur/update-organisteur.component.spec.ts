import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateOrganisteurComponent } from './update-organisteur.component';

describe('UpdateOrganisteurComponent', () => {
  let component: UpdateOrganisteurComponent;
  let fixture: ComponentFixture<UpdateOrganisteurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateOrganisteurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateOrganisteurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
