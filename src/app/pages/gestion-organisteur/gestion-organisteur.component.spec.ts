import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionOrganisteurComponent } from './gestion-organisteur.component';

describe('GestionOrganisteurComponent', () => {
  let component: GestionOrganisteurComponent;
  let fixture: ComponentFixture<GestionOrganisteurComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionOrganisteurComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionOrganisteurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
