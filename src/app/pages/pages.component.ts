import { Component, OnInit } from '@angular/core';

import { MENU_ADMIN, MENU_GUEST, MENU_ORGANISATEUR, MENU_SPORTIF } from './pages-menu';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent implements OnInit {

  menu;
  ngOnInit(): void {
    if(localStorage.getItem("role") == "ROLE_GUEST"){
      this.menu = MENU_GUEST;
    }
    if(localStorage.getItem("role") == "ROLE_SPORT"){
      this.menu = MENU_SPORTIF;
    }
    if(localStorage.getItem("role") == "ROLE_USER"){
      this.menu = MENU_ORGANISATEUR;
    }
    if(localStorage.getItem("role") == "ROLE_ADMIN"){
      this.menu = MENU_ADMIN;
    }
  }
}
