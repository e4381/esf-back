import { Component, OnInit } from '@angular/core';
import { CalendarOptions } from '@fullcalendar/angular';
import { MessageService } from 'primeng/api';
import { AuthService } from '../../service/auth/auth.service';
import { CompetitionResultService } from '../../service/competition-result/competition-result.service';

@Component({
  selector: 'ngx-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {

  constructor(
    private competitionResultService: CompetitionResultService,
    private authService: AuthService,
    private messageService: MessageService) { }

  events: any[] = [];
  options: CalendarOptions;
  header: any;
  display:boolean =false;
  currentPartisipation: string;
  filter: string;
  allEvent : any[] = [];
  partisipations: any[] = [];

  ngOnInit(): void {
    if(this.isAdmin()) {
      this.findAllEvent()
    }else{
      this.findEvent();
    }
  }

  setCalender() {
    this.options = {
      initialDate: new Date(),
      headerToolbar: {
        left: 'prev,next today',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
      },
      eventClick: this.handleDateClick.bind(this),
      events: this.events,
      editable: false,
      selectable: true,
      selectMirror: true,
      dayMaxEvents: true
    };
  }

  filterEvent() {
    if(!!this.filter){
      this.events = this.events.filter(event => event.title.includes(this.filter));
    }else{
      this.events = [...this.allEvent];
    }    
    this.setCalender();
  }
  findEvent() {
    this.competitionResultService.findAllByUserId(this.authService.getUserId()).subscribe(val => {
      console.log(val);
      this.partisipations = val; 
      val.forEach(element => {
        this.events.push({
          title: element.competition.name,
          start: element.competition.startEvent,
          end: element.competition.endEvent,
          data: element,
          publicId: element.id,
          groupId: element.id,
          color: null
        })
      })
      this.allEvent = [...this.events];
      this.setCalender();
    })
  }

  findAllEvent() {
    this.competitionResultService.findAllCompetitionResult().subscribe(val => {
      this.partisipations = val; 
      val.forEach(element => {
        this.events.push({
          title: element.competition.name,
          start: element.competition.startEvent,
          end: element.competition.endEvent,
          data: element,
          publicId: element.id,
          groupId: element.id,
          color: null
        })
      })
      this.allEvent = [...this.events];
      this.setCalender();
    })
  }

  confirme() {
    if(this.isValidDate()){
      this.addNotif('error', "Votre participation ne peux pas etre retirée carl'event est terminé"); 
      this.display = false;
      return;
    }
    this.competitionResultService.deletePartisipation(this.currentPartisipation).subscribe(val =>{
      this.events = this.events.filter(el =>el.groupId != this.currentPartisipation);
      this.display = false;
      this.allEvent = [...this.events];
      this.setCalender();
    })
  }

  handleDateClick(arg) {
    console.log(arg.event._def);
    this.currentPartisipation = arg.event._def.groupId;
    if(!this.isAdmin())
      this.display = true;
  }

  isValidDate() {
    let partisipation = this.partisipations.find(el => el.id == this.currentPartisipation)
    let date = new Date(partisipation.competition.endEvent);
    return !(date?.getTime() >= new Date().getTime());
  }

  addNotif(type: string, summary: string) {
    this.messageService.add({ severity: type, summary: summary });
  }

  isAdmin(){
    return localStorage.getItem("role") == "ROLE_ADMIN" ;
  }
}
