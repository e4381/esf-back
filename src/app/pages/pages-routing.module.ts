import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ECommerceComponent } from './e-commerce/e-commerce.component';
import { NotFoundComponent } from './miscellaneous/not-found/not-found.component';
import { CompetitionComponent } from './competition/competition.component';
import { CreateCompetitionComponent } from './competition/create-competition/create-competition.component';
import { CalendarComponent } from './calendar/calendar.component';
import { GroupComponent } from './group/group.component';
import { CreateGroupComponent } from './group/create-group/create-group.component';
import { GestionSportifComponent } from './gestion-sportif/gestion-sportif.component';
import { CreateSportifComponent } from './gestion-sportif/create-sportif/create-sportif.component';
import { UpdateSportifComponent } from './gestion-sportif/update-sportif/update-sportif.component';
import { GestionOrganisteurComponent } from './gestion-organisteur/gestion-organisteur.component';
import { CreateOrganisteurComponent } from './gestion-organisteur/create-organisteur/create-organisteur.component';
import { UpdateOrganisteurComponent } from './gestion-organisteur/update-organisteur/update-organisteur.component';
import { CompetitionMapComponent } from './competition/competition-map/competition-map.component';
import { UserProfilComponent } from './user-profil/user-profil.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: 'dashboard',
      component: ECommerceComponent,
    },
    {
      path: 'gestionCompetition',
      component: CompetitionComponent,
    },
    {
      path: 'gestionCompetition/create',
      component: CreateCompetitionComponent,
    },
    {
      path: 'gestionCompetition/update/:id',
      component: CreateCompetitionComponent,
    },
    {
      path: 'gestionCompetition/map',
      component: CompetitionMapComponent,
    },
    {
      path: 'calendar',
      component: CalendarComponent,
    },
    {
      path: 'gestionGroupe',
      component: GroupComponent,
    },
    {
      path: 'gestionGroupe/create',
      component: CreateGroupComponent,
    },
    {
      path: 'gestionGroupe/update/:id',
      component: CreateGroupComponent,
    },
    {
      path: 'gestionSportif',
      component: GestionSportifComponent,
    },{
      path: 'gestionSportif/create',
      component: CreateSportifComponent,
    },
    {
      path: 'gestionSportif/update/:id',
      component: UpdateSportifComponent,
    },
    {
      path: 'gestionOrganisteur',
      component: GestionOrganisteurComponent,
    },{
      path: 'gestionOrganisteur/create',
      component: CreateOrganisteurComponent,
    },
    {
      path: 'gestionOrganisteur/update/:id',
      component: UpdateOrganisteurComponent,
    },
    {
      path: 'GestionUserProfil',
      component: UserProfilComponent,
    },
    {
      path: '',
      redirectTo: 'gestionCompetition',
      pathMatch: 'full',
    },
    {
      path: '**',
      component: NotFoundComponent,
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {
}
