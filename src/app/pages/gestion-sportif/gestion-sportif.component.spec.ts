import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionSportifComponent } from './gestion-sportif.component';

describe('GestionSportifComponent', () => {
  let component: GestionSportifComponent;
  let fixture: ComponentFixture<GestionSportifComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GestionSportifComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionSportifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
