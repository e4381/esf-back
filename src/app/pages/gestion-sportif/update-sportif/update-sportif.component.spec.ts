import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateSportifComponent } from './update-sportif.component';

describe('UpdateSportifComponent', () => {
  let component: UpdateSportifComponent;
  let fixture: ComponentFixture<UpdateSportifComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateSportifComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateSportifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
