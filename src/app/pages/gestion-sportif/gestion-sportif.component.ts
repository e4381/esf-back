import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { User } from '../../model/User';
import { UserService } from '../../service/user/user.service';

@Component({
  selector: 'ngx-gestion-sportif',
  templateUrl: './gestion-sportif.component.html',
  styleUrls: ['./gestion-sportif.component.scss']
})
export class GestionSportifComponent implements OnInit {

  constructor(private userService: UserService,
    private route: Router,
    private messageService: MessageService) { }

  users: User[] = [];
  loading: boolean = true;
  diplay: boolean = false;
  displayDetails: boolean = false;
  currnetUser: User;

  cols: any[] = [
    { field: "name", header: "name" },
    { field: "sportType", header: "sportType" },
    { field: "email", header: "email" },
    { field: "email", header: "email" },
    { field: "Action", header: "Action" }];

  ngOnInit(): void {
    this.getSportfs();
  }

  getSportfs() {
    this.userService.findAllUserByRole("ROLE_SPORT").subscribe(res => {
      this.users = res;
      this.loading = false;
    })
  }

  addSportif() {
    this.route.navigateByUrl("/pages/gestionSportif/create")
  }

  editSportif(TrainingGroup: User) {
    this.route.navigateByUrl("/pages/gestionSportif/update/" + TrainingGroup?.id)
  }

  deleteSportif(user: User) {
    this.currnetUser = user;
    this.diplay = true;
  }

  confirme() {
    this.userService.deleteUserById(this.currnetUser.id).subscribe(val =>{
      console.log(val);
      this.users = this.users.filter(el => el.id != this.currnetUser.id);
      this.diplay = false;
    })
  }

  disable(user: User) {
    this.userService.disableUser(user.id).subscribe(val =>{
      this.addNotif('success', "ce sportif a été bloqué "); 
      user.isActive = false;
    })
  }

  enableUser(user: User) {
    this.userService.enableUser(user.id).subscribe(val =>{
      this.addNotif('success', "ce sportif a été débloqué "); 
      user.isActive = true;
    })
  }

  addNotif(type: string, summary: string) {
    this.messageService.add({ severity: type, summary: summary });
  }

}
