import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateSportifComponent } from './create-sportif.component';

describe('CreateSportifComponent', () => {
  let component: CreateSportifComponent;
  let fixture: ComponentFixture<CreateSportifComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CreateSportifComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateSportifComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
