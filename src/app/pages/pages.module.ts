import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';
import { FullCalendarModule } from '@fullcalendar/angular';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import timeGridPlugin from '@fullcalendar/timegrid';
import { NbCardModule, NbInputModule, NbMenuModule, NbSelectModule, NbSpinnerModule, NbTabsetModule } from '@nebular/theme';
import { MessageService } from 'primeng/api';
import { CalendarModule } from 'primeng/calendar';
import { DialogModule } from 'primeng/dialog';
import { RatingModule } from 'primeng/rating';
import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { ToastModule } from 'primeng/toast';
import { ThemeModule } from '../@theme/theme.module';
import { CalendarComponent } from './calendar/calendar.component';
import { CommentComponent } from './competition/comment/comment.component';
import { CompetitionMapComponent } from './competition/competition-map/competition-map.component';
import { CompetitionComponent } from './competition/competition.component';
import { CreateCompetitionComponent } from './competition/create-competition/create-competition.component';
import { DetailsCompetitionComponent } from './competition/details-competition/details-competition.component';
import { DashboardModule } from './dashboard/dashboard.module';
import { ECommerceModule } from './e-commerce/e-commerce.module';
import { CreateOrganisteurComponent } from './gestion-organisteur/create-organisteur/create-organisteur.component';
import { GestionOrganisteurComponent } from './gestion-organisteur/gestion-organisteur.component';
import { UpdateOrganisteurComponent } from './gestion-organisteur/update-organisteur/update-organisteur.component';
import { CreateSportifComponent } from './gestion-sportif/create-sportif/create-sportif.component';
import { GestionSportifComponent } from './gestion-sportif/gestion-sportif.component';
import { UpdateSportifComponent } from './gestion-sportif/update-sportif/update-sportif.component';
import { CreateGroupComponent } from './group/create-group/create-group.component';
import { GroupComponent } from './group/group.component';
import { MapsModule } from './maps/maps.module';
import { MiscellaneousModule } from './miscellaneous/miscellaneous.module';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages.component';
import { ChangePasswordComponent } from './user-profil/change-password/change-password.component';
import { ListeCompetitionsComponent } from './user-profil/liste-competitions/liste-competitions.component';
import { ProfilComponent } from './user-profil/profil/profil.component';
import { UserProfilComponent } from './user-profil/user-profil.component';


FullCalendarModule.registerPlugins([
  dayGridPlugin,
  timeGridPlugin,
  interactionPlugin
]);

@NgModule({
  imports: [
    PagesRoutingModule,
    ThemeModule,
    NbMenuModule,
    DashboardModule,
    ECommerceModule,
    MiscellaneousModule,
    TableModule,
    NbCardModule,
    NbInputModule,
    ReactiveFormsModule,
    CalendarModule,
    NbSpinnerModule,
    NbSelectModule,
    DialogModule,
    RatingModule,
    FormsModule,
    ToastModule,
    FullCalendarModule,
    NbTabsetModule,
    LeafletModule.forRoot(),
    TabViewModule,
    MapsModule
  ],
  declarations: [
    PagesComponent,
    CompetitionComponent,
    CreateCompetitionComponent,
    DetailsCompetitionComponent,
    CommentComponent,
    CalendarComponent,
    GroupComponent,
    CreateGroupComponent,
    GestionSportifComponent,
    GestionOrganisteurComponent,
    CreateSportifComponent,
    UpdateSportifComponent,
    CreateOrganisteurComponent,
    UpdateOrganisteurComponent,
    CompetitionMapComponent,
    UserProfilComponent,
    ProfilComponent,
    ChangePasswordComponent,
    ListeCompetitionsComponent
  ],
  providers: [MessageService],
})
export class PagesModule {
}
