import { Competition } from "./Competition";
import { User } from "./User";

export class Comment {
    id: string;
    user: User;
    competition: Competition;
    commentDate: Date;
    message: String;
}