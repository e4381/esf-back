export class Rss {
    title: string;
    description: string;
    link: string;
    enclosure: string;
    pubDate: string;
}