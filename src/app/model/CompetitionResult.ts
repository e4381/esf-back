import { Competition } from "./Competition";
import { User } from "./User";

export class CompetitionResult {
    id: string;
    user: User;
    competition: Competition;
    score: number;
}