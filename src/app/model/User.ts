import { TrainingGroup } from "./TrainingGroup";

export class User {
    id: string;
    createdDate: Date;
    isActive: boolean;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    adresse: string;
    login: string;
    role: string;
    sportType: string;
    trainingGroup: TrainingGroup;
    logo: string;

}