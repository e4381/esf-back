export class Competition {
    id: string;
    name: string;
    description: string;
    sportType: string;
    startEvent: string;
    endEvent: string;
    latitude: string;
    longitude: string;
    participated?: any;
    logo: string;
}