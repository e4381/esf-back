import { User } from "./User";

export class TrainingGroup {
    id: string;
    name: string;
    description: string;
    sportType: string;
    logo: string;
    joined?: User;
}