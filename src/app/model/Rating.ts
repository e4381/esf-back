import { Competition } from "./Competition";
import { User } from "./User";

export class Rating {
    id: string;
    user: User;
    competition: Competition;
    ratingDate: Date;
    number: number = 0;
}